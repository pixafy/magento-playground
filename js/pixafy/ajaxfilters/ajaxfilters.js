/**
 * Pixafy AjaxFilters
 *
 * Since AJAX requires JS (duh), this file will listen for DOM elements and replace necessary 
 * links with a click event. This prevents unnecessary design changes/conflicts when
 * rewriting Magento Block and template files (plug-and-play friendly). If AJAX is unavailable
 * (JS disabled) then the module will not interfere with any native backwards compatibility
 *
 * @category    Pixafy
 * @package     Pixafy_AjaxFilters
 * @copyright   Copyright (c) 2013 Pixafy (http://www.pixafy.com)
 * @author      Thomas Lackemann
 */
(function() {
	'use strict'

	var AjaxFilter = function() {

		/**
		 * Reference to self
		 * @var AjaxFilter
		 */
		var self = this;

		/**
		 * DOM block class/id of the sidebar (the root element of the sidebar block defined in System > Configuration > Ajax Filters)
		 * @var string
		 */
		this.elementSidebar = '.block-layered-nav',

		/**
		 * DOM block class/id of the sidebar (the root element of the products block defined in System > Configuration > Ajax Filters)
		 * @var string
		 */
		this.elementProducts = '.category-products',

		/**
		 * Element's data attribute of the ajax url
		 * @var string
		 */
		this.attributeAjaxUrl = 'data-href',

		/**
		 * DOM block id of the narrow by list (normally, this won't change)
		 * @var string
		 */
		this.elementNarrowByList = 'narrow-by-list',

		/**
		 * Starts/reloads the processes necessary for the AjaxFilter
		 * @param boolean documentLoaded
		 * @return AjaxFilter
		 */
		this.init = function(documentLoaded) {
			if (documentLoaded === null) {
				documentLoaded = true;
			}

			if (documentLoaded) {
				this._init();
			} else {
				// Wait for document load
				document.observe("dom:loaded", function() {
					self._init();
				});
			}

			return this;
		},

		/**
		 * Adds processes to any necessary elements
		 * @return AjaxFilter
		 */
		this._init = function() {

			// Check if we need to process filter links
			if($(this.elementNarrowByList) !== null) {

				// Add a click listener to the filter links
				$(this.elementNarrowByList).select('a').each(function(element) {
					self.addClickListener(element);
				});
			}

			// Check if we need to process remove/clear all buttons
			if ($$('.btn-remove').length > 0) {

				// Add a click listener to the remove buttons
				$$('.btn-remove').each(function(element) {
					self.addClickListener(element);
				});

				// Add click listener to the action button (clear all)
				$$('.actions').each(function(actionsElement) {
					actionsElement.select('a').each(function(element) {
						self.addClickListener(element);
					});
				});
			}
		},

		/**
		 * Adds a click listener to the element.
		 * The click listener will make an ajax call to the original category url which has been
		 * modified to return a JSON array of blocks - The new blocks will replace the original
		 * blocks, ensuring a consistent design
		 * 
		 * @param Element element
		 * @return boolean|AjaxFilter
		 */
		this.addClickListener = function(element) {
			if (!element) {
				return false;
			}

			// Set a custom data attribute and replace the href with js
			element.setAttribute(this.attributeAjaxUrl, element.href);
			element.href = 'javascript:void(0)';

			$(element).observe('click', function(event) {
				// Stop propogation
				event.stop();

				// Send an ajax call
				var ajaxUrl = (element.getAttribute(self.attributeAjaxUrl)) ? element.getAttribute(self.attributeAjaxUrl) : element.href;
				new Ajax.Request(ajaxUrl, {
					method: 'GET',
					onSuccess: function(data) {
						// Handle the json
						self._initClickSuccess(data)
					},
					onFailure: function(data) {
						/** @todo handle error on page */
					}
				});
			});

			return this;
		},

		/**
		 * Replaces and reloads the sidebar and product list with the new returned HTML
		 * @return AjaxFilter
		 */
		this._initClickSuccess = function(data) {

			if (data.responseJSON !== null) {

				// Replace the URL (the HTML5 way)
				window.history.pushState(null, null, data.request.url);
				
				// Replace the sidebar
				if (data.responseJSON.sidebar !== undefined && data.responseJSON.sidebar != '') {
					$$(this.elementSidebar).each(function(elem) {
						elem.innerHTML = data.responseJSON.sidebar;
					});
				}

				// Replace the main content
				if (data.responseJSON.sidebar !== undefined && data.responseJSON.products != '') {
					$$(this.elementProducts).each(function(elem) {
						elem.innerHTML = data.responseJSON.products;
					});
				}

				this.init(true);
			}

			return this;	
		}

		// Start the processes on instantiation
		this.init(false);
		return this;
	}

	// Bind the object to our window
	if (!window.AjaxFilter) {
		window.AjaxFilter = new AjaxFilter();
	} else {
		window._AjaxFilter = new AjaxFilter();
	}
})();