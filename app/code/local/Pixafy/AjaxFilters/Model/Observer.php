<?php
/**
 * Pixafy AjaxFilters
 *
 * @category	Pixafy
 * @package		Pixafy_AjaxFilters
 * @copyright	Copyright (c) 2013 Pixafy (http://www.pixafy.com)
 * @author 		Thomas Lackemann
 */

class Pixafy_AjaxFilters_Model_Observer
{
	/**
	 * Add the ajaxfilters.js file when enabled
	 *
	 * @param Varien_Event_Observer
     * @return Mage_Sales_Model_Observer
	 */
	public function controllerActionLayoutRenderBeforeAction($observer = null)
	{
		if (Mage::getStoreConfig(Pixafy_AjaxFilters_Helper_Data::XML_PATH_AJAXFILTERS_ENABLED))
		{
			$layout = Mage::app()->getLayout()->getBlock('head');
			$layout->addJs('pixafy/ajaxfilters/ajaxfilters.js');
		}
	}
}