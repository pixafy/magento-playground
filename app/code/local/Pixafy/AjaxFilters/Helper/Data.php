<?php
/**
 * Pixafy AjaxFilters
 *
 * @category	Pixafy
 * @package		Pixafy_AjaxFilters
 * @copyright	Copyright (c) 2013 Pixafy (http://www.pixafy.com)
 * @author 		Thomas Lackemann
 */

class Pixafy_AjaxFilters_Helper_Data extends Mage_Core_Helper_Abstract
{
	/**
	 * Constants
	 */
	const XML_PATH_AJAXFILTERS_ENABLED						=	'ajaxfilters/general/is_enabled';
	const XML_PATH_AJAXFILTERS_CATALOGSEARCH_SIDEBAR_BLOCK	=	'ajaxfilters/catalogsearch/sidebar_block';
	const XML_PATH_AJAXFILTERS_CATALOGSEARCH_PRODUCTS_BLOCK	=	'ajaxfilters/catalogsearch/products_block';
	const XML_PATH_AJAXFILTERS_CATALOG_SIDEBAR_BLOCK		=	'ajaxfilters/catalog/sidebar_block';
	const XML_PATH_AJAXFILTERS_CATALOG_PRODUCTS_BLOCK		=	'ajaxfilters/catalog/products_block';

    /**
     * Returns a JSON encoded response of blocks
     * 
     * @param array $blocks
     * @return Pixafy_AjaxFilters_Controller_Action
     */
    public function returnJson($blocks = array())
    {
        $response = Mage::app()->getResponse();
        $response->setHeader('Content-type', 'application/json');

        $data = array();
        foreach($blocks as $key => $block)
        {
        	// Ensure we're returning a valid block
            $data[$key] = ($block instanceof Mage_Core_Block_Template) ? $block->toHtml() : null;
        }
        $jsonData = json_encode($data);
        $response->setBody($jsonData);
        return;
    }
}