<?php
/**
 * Pixafy AjaxFilters
 *
 * @category	Pixafy
 * @package		Pixafy_AjaxFilters
 * @copyright	Copyright (c) 2013 Pixafy (http://www.pixafy.com)
 * @author 		Thomas Lackemann
 */

require_once(MAGENTO_ROOT.DS.'app'.DS.'code'.DS.'core'.DS.'Mage'.DS.'Catalog'.DS.'controllers'.DS.'CategoryController.php');
class Pixafy_AjaxFilters_Catalog_CategoryController extends Mage_Catalog_CategoryController
{
	/**
     * Category view action
     */
    public function viewAction()
    {
        // Let the parent do most of the work (while not optimized, it does provide us with an easy way to plug-and-play)
        parent::viewAction();

    	if (Mage::getStoreConfig(Pixafy_AjaxFilters_Helper_Data::XML_PATH_AJAXFILTERS_ENABLED) && $this->getRequest()->isAjax())
        {
            // Get sidebar and product list child blocks from layout (as to not impose conflicts across designs)
            $blocks = array(
                'sidebar'   => null,
                'products'  => null,
            );

            // Get the sidebar block
            $blocks['sidebar'] = $this->getLayout()->getBlock(Mage::getStoreConfig(Pixafy_AjaxFilters_Helper_Data::XML_PATH_AJAXFILTERS_CATALOG_SIDEBAR_BLOCK));
            // Get the product list block
            $blocks['products'] = $this->getLayout()->getBlock(Mage::getStoreConfig(Pixafy_AjaxFilters_Helper_Data::XML_PATH_AJAXFILTERS_CATALOG_PRODUCTS_BLOCK));

            // Return json
            Mage::helper('ajaxfilters')->returnJson($blocks);
        }
    }
}