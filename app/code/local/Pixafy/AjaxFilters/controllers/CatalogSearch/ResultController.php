<?php
/**
 * Pixafy AjaxFilters
 *
 * @category	Pixafy
 * @package		Pixafy_AjaxFilters
 * @copyright	Copyright (c) 2013 Pixafy (http://www.pixafy.com)
 * @author 		Thomas Lackemann
 */

require_once(MAGENTO_ROOT.DS.'app'.DS.'code'.DS.'core'.DS.'Mage'.DS.'CatalogSearch'.DS.'controllers'.DS.'ResultController.php');
class Pixafy_AjaxFilters_CatalogSearch_ResultController extends Mage_CatalogSearch_ResultController
{
	/**
     * Display search result
     */
    public function indexAction()
    {
        // Let the parent do most of the work (while not optimized, it does provide us with an easy way to plug-and-play)
        parent::indexAction();

    	if (Mage::getStoreConfig(Pixafy_AjaxFilters_Helper_Data::XML_PATH_AJAXFILTERS_ENABLED) && $this->getRequest()->isAjax())
    	{
    		// Get sidebar and product list child blocks from layout (as to not impose conflicts across designs)
    		$blocks = array(
    			'sidebar'	=> null,
    			'products'	=> null,
    		);

            // Get the sidebar block
            $blocks['sidebar'] = $this->getLayout()->getBlock(Mage::getStoreConfig(Pixafy_AjaxFilters_Helper_Data::XML_PATH_AJAXFILTERS_CATALOGSEARCH_SIDEBAR_BLOCK));
            // Get the product list block
            $blocks['products'] = $this->getLayout()->getBlock(Mage::getStoreConfig(Pixafy_AjaxFilters_Helper_Data::XML_PATH_AJAXFILTERS_CATALOGSEARCH_PRODUCTS_BLOCK));

            // Return json
    		Mage::helper('ajaxfilters')->returnJson($blocks);
    	}
    }
}